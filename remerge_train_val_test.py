#!/usr/bin/python

import math
import numpy as np
import os
import subprocess as sbp
from os.path import isfile, join, isdir
from os import listdir



def remerge_trainvaltest():
    
  alreadyfile="alreadySPLIT.out"
  exists=os.path.isfile(alreadyfile)#.is_file():
  
  target_folders=["flower_photos/"]
  source_folders=["test_flower_photos/","val_flower_photos/"]
  
  if exists:
    sbp.call("rm "+alreadyfile, shell=True)
  
  for itar,target_folder in enumerate(target_folders):
    #cmdstr="mkdir "+target_folder
    #sbp.call(cmdstr,shell=True)
    for sfold in source_folders:
  
      sourceSubdirs=[f for f in listdir(sfold) if isdir(join(sfold, f))]
      for sd in sourceSubdirs:
          
        folder=sfold+sd+"/"
        fileLs=[f for f in listdir(folder) if isfile(join(folder, f))]
        nF=len(fileLs)
        #ranvec=np.random.randint(0,high=nF,size=int(nF*split[itar]),dtype=int)
  
  
        cmdstr="mkdir "+target_folder+sd
        sbp.call(cmdstr,shell=True)
        for ifile,fil in enumerate(fileLs):
          #if ifile in ranvec:
          cmdstr="mv "+folder+fil+" "+target_folder+sd+"/."
          print(cmdstr)#
          #fwrite.write( cmdstr+"\n" )
          sbp.call(cmdstr,shell=True)
  
    #for sd in sourceSubdirs:
    #    fwrite.write( sd )
  #fwrite.close()

if __name__=="__main__":
  remerge_trainvaltest()