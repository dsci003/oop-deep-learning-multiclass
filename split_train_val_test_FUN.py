#!/usr/bin/python

import math
import numpy as np
import os
import subprocess as sbp
from os.path import isfile, join, isdir
from os import listdir



def split_train_val_test(splitdct={"test":10,"val":20}):
  # check whether splitting is already done
  alreadyfile="alreadySPLIT.out"
  exists=os.path.isfile(alreadyfile)#.is_file():
  
  source_folder="flower_photos/"
  target_folders=["test_flower_photos/","val_flower_photos/"]

  
  split=[splitdct["test"]/100.0,spltdct["val"]/(1.0-splitdct["test"])]

  if exists == 1 :
      fread=open(alreadyfile,"r")
  
      for l in fread:
          print(l)
  
      fread.close
      return "train-val-test division has already been done"
      #import sys
      #sys.exit(" 'aldeadySPLIT.out' safety file is in place, aborting ")


  #exists=os.path.isfile(alreadyfile)
  fwrite=open(alreadyfile,"w")
  sourceSubdirs=[f for f in listdir(source_folder) if isdir(join(source_folder, f))]
  for itar,target_folder in enumerate(target_folders):
  	cmdstr="mkdir "+target_folder
  	sbp.call(cmdstr,shell=True)

  	for sd in sourceSubdirs:
  		pass
  		folder=source_folder+sd+"/"
  		fileLs=[f for f in listdir(folder) if isfile(join(folder, f))]
  		nF=len(fileLs)
  		ranvec=np.random.randint(0,high=nF,size=int(nF*split[itar]),dtype=int)
  
  
  		cmdstr="mkdir "+target_folder+sd
  		sbp.call(cmdstr,shell=True)
  		for ifile,fil in enumerate(fileLs):
  			if ifile in ranvec:
  				cmdstr="mv "+folder+fil+" "+target_folder+sd+"/."
  				print(cmdstr)#
  				fwrite.write( cmdstr+"\n" )
  				sbp.call(cmdstr,shell=True)
  
  	#for sd in sourceSubdirs:
  	#    fwrite.write( sd )
  fwrite.close()


if __name__=="__main__":
  split_train_val_test()